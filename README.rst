drill-srs-plus
==============

A CLI program for learning things through `spaced repetition
<https://en.wikipedia.org/wiki/Spaced_repetition>`__. It is a GPL-licensed
fork of Marcin Kurczewski’s `drill-srs <https://github.com/rr-/drill>`__ that
implements the option to show a hint text and to play a hint audio file if
they are available when reviewing (flash)cards.

Features
--------

-  CLI (ideal for ``tmux``\ +\ ``ssh``)
-  Multiple decks
-  TODO Show hint/help text [see ``develop`` branch for proof-of-concept]
-  TODO Play (hint) audio file [see ``develop`` branch for proof-of-concept]
-  No configuration needed
-  Colourful tags
-  JSON exports/imports for easy deck manipulation
-  HTML reports

Screenshot
----------

TODO

Installation
------------

::

   git clone https://gitlab.com/beneklins/drill-srs-plus
   cd drill-srs-plus
   pip install . --upgrade

Then run ``drill-srs-plus`` to see the available commands.

How to use
----------

Flashcards are organized in decks, so that you can study multiple subjects at
once. To start studying, first create a deck and populate it with cards
manually or through an import.

Each “study session” will then display the cards to memorise. Following the
study, you’ll be invited to a “review session” of all cards shown so far,
where your answers will affect how often a given card will be shown for
re-evaluation. A correct answer increases the card’s score by 1, while a
mistake decreases its score by 1.

This is how the score translates into the re-evaluation delay:

========== ===================================
Card score Wait time
========== ===================================
0          none (just after the study session)
1          1 hour
2          3 hours
3          8 hours
4          1 day
5          3 days
6          1 week
7          2 weeks
8          1 month
9          2 months
10         4 months
========== ===================================

For example, if you answered the given card correctly thrice (score 3) and now
made a mistake (score is 2 now), this card will re-appear after 8 hours. The
score can’t fall outside the range in the table.

Such review system reinforces the quality of the memorisation.

Questions
---------

**Q: Why not contribute directly to ``drill-srs``?** A: The original software
is released under the MIT licence, which permits reuse within proprietary
software. I prefer to keep things open. Therefore, my changes (this fork) are
released under the GNU Public Licence v3 or later.

**Q: Why not Anki?** A: This was a design decision from the `original author/
program <https://github.com/rr-/drill/blob/master/README.md#Questions>`__.
Moreover, it keep things simple.

**Q: Why not SuperMemo or other better algorithms?** A: Once again, this comes
from a design decision from the original author/program.

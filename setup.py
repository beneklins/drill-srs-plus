from setuptools import find_packages, setup

setup(
    author="Igor Benek-Lins",
    author_email="beneklins@protonmail.ch",
    name="drill-srs-plus",
    long_description="Spaced repetition learning in CLI",
    version="0.3",
    url="https://gitlab.com/beneklins/drill-srs-plus",
    packages=find_packages(),
    entry_points={"console_scripts": ["drill-srs-plus = drillsrs.__main__:main"]},
    install_requires=["xdg", "python-dateutil", "sqlalchemy", "jinja2",],
    package_dir={"drill-srs-plus": "drillsrs"},
    package_data={"drill-srs-plus": ["data/*.*"]},
    classifiers=[
        "Environment :: Console",
        "Development Status :: 4 - Beta",
        "Intended Audience :: Education",
        "License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3 :: Only",
        "Topic :: Education",
    ],
)
